import datetime
import json
import os.path
import re
import socket
import sys
import time

import pymysql


class Server:
    def __init__(self, port):
        self.sock = socket.socket()
        self.port = port
        self.conn = None
        self.addr = None
        self.docs_dbh = None
        self.cursor = None
        self.archive_folder = "/media/andrey/LinuxHDD/doc_archive/"
        self.get_destinations = {
            "/doc_types": self.get_doc_types,
            "/position_types": self.get_position_types
        }
        self.post_destinations = {
            "/auth": self.auth,
            "/docs": self.get_docs,
            "/agreed_docs_folder": self.agreed_docs_folder,
            "/add_document": self.add_document,
            "/add_user": self.add_user,
            "/get_doc_html": self.get_doc_html,
            "/is_receiver_exists": self.is_receiver_exists,
            "/add_new_revision": self.set_doc_html,
            "/agree_doc": self.agree_doc,
            "/get_note": self.get_note,
            "/get_agreement_history": self.get_agreement_history,
            "/get_agreement_revision": self.get_agreement_revision
        }
        self.methods = {
            "GET": self.get_destinations,
            "POST": self.post_destinations
        }
        self.error_codes = {
            "MySql connection error": 1
        }
        self.connect_to_db()

    def connect_to_db(self):
        try:
            self.docs_dbh = pymysql.connect(host='localhost', port=3306, user='root', password='password',
                                            charset="utf8",
                                            db='easydm')
            self.cursor = self.docs_dbh.cursor()
            print("Connected to MySql")
        except Exception as e:
            print("Connection to MySQL failed:" + str(e))
            exit(self.error_codes.get("MySql connection error"))

    def start_server(self):
        while True:
            try:
                self.sock.bind(('', self.port))
                break
            except OSError:
                time.sleep(1)
        self.sock.listen(5)
        print("Server started")
        try:
            while True:
                self.conn, self.addr = self.sock.accept()
                self.connection_processor()
        except KeyboardInterrupt as e:
            self.stop_server(e)

    def connection_processor(self):
        request = self.conn.recv(4096).decode('utf-8', errors='ignore')
        request_method = request.split()[0]
        destination = request.split()[1]
        try:
            if request_method == "POST":
                pattern = r"name=\"(.*)\"\s*Content-Length:\s*(\d*)\s*([^\r]*)"
                request = re.sub(r'[\n\r]]+', ' ', request)
                params_tpl = re.findall(pattern, request)
                params = {item[0]: item[2] for item in params_tpl}
                self.methods.get(request_method).get(destination)(params)
            else:
                self.methods.get(request_method).get(destination)()
        except Exception as e:
            print(str(e))
            self.stop_server(e)

    def get_docs(self, params):
        try:
            print("POST get_docs processing")
            query = "SELECT document.document_id, document_name, agreement_date, employee.employee_name, \
                      agreement.agreement_id, employee.email FROM agreement, document, employee WHERE employee.email='{}' \
                      and document.document_id=agreement.document_id and agreement.employee_id=employee.employee_id\
                      and agreement.agreement_result='not agreed'\
                      ORDER BY agreement.agreement_id".format(
                params.get('email'))
            self.cursor.execute(query)
            db_response = self.cursor.fetchall()
            formatted_response = {item[0]:
                {
                    "doc_name": item[1],
                    "customer": item[3],
                    "create_date": item[2],
                    "agreement_id": item[4],
                    "customer_email": item[5]
                } for item in db_response}
            result = {
                "error": 0,
                "response": formatted_response
            }
            self.send_response(200, result)
        except Exception as e:
            print(str(e))
            self.send_response(500, {"error": str(e)})

    def agreed_docs_folder(self, params):
        try:
            print("POST agreed_docs_folder processing")
            query = "SELECT document.document_id, document_name, agreement_date, employee.employee_name, \
                      agreement.agreement_id, employee.email FROM agreement, document, employee WHERE employee.email='{}' \
                      and document.document_id=agreement.document_id and agreement.employee_id=employee.employee_id\
                      and agreement.agreement_result='agreed'\
                      ORDER BY agreement.agreement_id".format(
                params.get('email'))
            self.cursor.execute(query)
            db_response = self.cursor.fetchall()
            formatted_response = {item[0]:
                {
                    "doc_name": item[1],
                    "customer": item[3],
                    "create_date": item[2],
                    "agreement_id": item[4],
                    "customer_email": item[5]
                } for item in db_response}
            result = {
                "error": 0,
                "response": formatted_response
            }
            self.send_response(200, result)
        except Exception as e:
            print(str(e))
            self.send_response(500, {"error": str(e)})

    def add_document(self, params):
        print("add_document processing")
        query = "INSERT INTO document (document_type_id, document_name, document_desc) VALUE ('{}', '{}', '{}')" \
            .format(params.get("doc_type_id"), params.get("doc_name"), params.get("doc_desc"))
        try:
            self.cursor.execute(query)
            self.docs_dbh.commit()
            self.cursor.execute("SELECT document_id FROM document ORDER BY document_id DESC LIMIT 1")
            document_id = self.cursor.fetchall()
            params["document_id"] = document_id[0][0]
            self.add_agreement(params)
        except Exception as e:
            print(str(e))
            self.send_response(500, {"error": str(e)})

    def add_user(self, params):
        query = """INSERT INTO employee(employee_name, position_id, login, pass, phone, email)
                      VALUES ('%s', '%d', '%s', '%s', '%s', '%s')""" % \
                (params.get('name'),
                 int(params.get('position_id')),
                 params.get('login'),
                 params.get('pass'),
                 params.get('phone'),
                 params.get('email'))
        try:
            self.cursor.execute(query)
            self.docs_dbh.commit()
            self.send_response(200, {'error': 0})
        except Exception as e:
            print(str(e))
            self.send_response(500, {'error': 0})

    def add_agreement(self, params):
        print("add_agreement processing")
        if params.get("document_id") is not None:
            self.cursor.execute("SELECT final_user FROM agreement WHERE document_id='{}'"
                                .format(params.get("document_id")))
            last_user = self.cursor.fetchall()
            if len(last_user) == 0:
                if params.get("sender", -1) != -1 and params.get("sender") != params.get("employee_id"):
                    last_user = params.get("employee_id")
                else:
                    last_user = None
            else:
                last_user = last_user[0][0]
                last_user = params.get("employee_id") if last_user is None else last_user
            self.del_last_agreement(params)
            if last_user is not None:
                query = "INSERT INTO agreement (document_id, employee_id, final_user, agreement_date, agreement_result) VALUES \
                        ('{}', '{}', '{}', '{}', '{}')".format(params.get("document_id"),
                                                               params.get("employee_id"),
                                                               last_user,
                                                               str(datetime.datetime.now()),
                                                               "not agreed")
            else:
                query = "INSERT INTO agreement (document_id, employee_id, agreement_date, agreement_result) VALUES \
                                        ('{}', '{}', '{}', '{}')".format(params.get("document_id"),
                                                                         params.get("employee_id"),
                                                                         str(datetime.datetime.now()),
                                                                         "not agreed")
            try:
                self.cursor.execute(query)
                self.docs_dbh.commit()
                if not params.get('noresponse', False):
                    self.send_response(200, {"error": 0})
                self.add_doc_to_archive(params)
            except Exception as e:
                print(str(e))
                self.send_response(500, {"error": str(e)})

    def del_last_agreement(self, params):
        query = "DELETE FROM agreement WHERE document_id='{}'".format(params.get("document_id"))
        try:
            self.cursor.execute(query)
            self.docs_dbh.commit()
        except Exception as e:
            print(str(e))
            pass

    def get_agreement_history(self, params):
        print("get_agreement_history processing")
        query = """SELECT revision_id, note, employee_name, email FROM documents_archive INNER JOIN employee e\
          on documents_archive.sender = e.employee_id WHERE document_id={} AND !(note='No comment' or note='New document' or note='Saving document')""".format(
            params.get("document_id"))
        try:
            self.cursor.execute(query)
            db_response = self.cursor.fetchall()
            result = {item[0]: {
                "note": item[1],
                "sender_name": item[2],
                "sender_email": item[3]
            } for item in db_response}
            self.send_response(200, {"error": 0, "response": result})
        except Exception as e:
            print(str(e))
            self.send_response(500, {"error": 1})

    def get_agreement_revision(self, params):
        query = """SELECT URL FROM documents_archive WHERE (document_id='{}' AND revision_id='{}')""". \
            format(params.get("document_id"), params.get("revision"))
        try:
            self.cursor.execute(query)
            with open(self.cursor.fetchall()[0][0], "r") as file:
                self.send_response(200, {"error": 0, "response": file.read()})
        except Exception as e:
            print(str(e))
            self.send_response(500, {"error": 1})

    def is_receiver_exists(self, params):
        query = "SELECT employee_id FROM employee where email='{}'".format(params.get("receiver_email"))
        self.cursor.execute(query)
        try:
            id = self.cursor.fetchall()[0][0]
            self.send_response(200, {"error": 0, "id": id})
        except Exception as e:
            print(str(e))
            self.send_response(200, {"error": 1})

    def add_doc_to_archive(self, params):
        print("add_doc_to_archive processing")
        try:
            last_agreement_id = self.get_last_agreement()
            sender = params.get("sender", params.get("employee_id"))
            query = "INSERT INTO documents_archive (document_id, URL, sender, note) VALUES ('{}', '{}', '{}', '{}')" \
                .format(params.get("document_id"),
                        self.archive_folder + "doc_" + str(params.get("document_id")) + "_" + str(last_agreement_id),
                        sender,
                        params.get("agreement_note", "New document"))
            self.cursor.execute(query)
            self.docs_dbh.commit()
        except Exception as e:
            print(str(e))
            self.send_response(500, {'error': 1})

    def get_last_agreement(self, **kwargs):
        try:
            if kwargs.get("id") is None:
                query = "SELECT agreement_id FROM agreement ORDER BY agreement_id DESC LIMIT 1"
            else:
                query = "SELECT agreement_id FROM agreement WHERE document_id='{}' ORDER BY agreement_id DESC LIMIT 1" \
                    .format(kwargs.get("id"))
            self.cursor.execute(query)
            return self.cursor.fetchall()[0][0]
        except Exception as e:
            print(str(e))
            self.send_response(500, {'error': 1})

    def get_doc_types(self):
        print("GET doc_types processing")
        query = "SELECT * FROM document_type"
        try:
            self.cursor.execute(query)
            db_response = self.cursor.fetchall()
            formatted_response = {item[0]: item[1] for item in db_response}
            result = {
                "error": 0,
                "response": formatted_response
            }
            self.send_response(200, result)
        except Exception as e:
            print(str(e))
            self.send_response(500, {"error": str(e)})

    def get_position_types(self):
        print("get_position_types processing")
        query = """SELECT position_id, position_name FROM position"""
        try:
            self.cursor.execute(query)
            response = self.cursor.fetchall()
            formatted_response = {item[0]: item[1] for item in response}
            self.send_response(200, {'error': 0, 'response': formatted_response})
        except Exception as e:
            print(str(e))
            self.send_response(500, {'error': 1})

    def auth(self, params):
        print("Auth request processing")
        try:
            query = "SELECT employee_id, pass, admin FROM easydm.employee WHERE email='{}'".format(params.get('email'))
            self.cursor.execute(query)
            db_response = self.cursor.fetchall()
            try:
                passwd = db_response[0][1]
                if passwd == params.get('pass'):
                    response = {
                        "error": 0,
                        'response': {
                            'auth_ok': 'true',
                            'employee_id': db_response[0][0],
                            'admin': db_response[0][2]
                        }
                    }
                else:
                    response = {
                        "error": "Wrong password",
                        'response': {
                            'auth_ok': 'false'
                        }
                    }
            except IndexError:
                response = {
                    "error": "Wrong email",
                    'response': {
                        'auth_ok': 'false'
                    }
                }

            self.send_response(200, response)
        except Exception as e:
            print(str(e))
            self.send_response(500, {'error': 1})

    def agree_doc(self, params):
        print("agree_doc processing")
        try:
            self.cursor.execute(
                "SELECT final_user FROM agreement WHERE document_id='{}'".format(params.get("document_id")))
            final_user_id = int(self.cursor.fetchall()[0][0])
            if int(params.get("user_id", -1)) == final_user_id:
                query = "UPDATE agreement SET agreement_result='agreed' where document_id='{}'" \
                    .format(params.get('document_id'))
                self.cursor.execute(query)
                self.docs_dbh.commit()
                self.send_response(200, {"error": 0})
            else:
                self.cursor.execute("UPDATE agreement SET employee_id='{}' WHERE document_id='{}'"
                                    .format(final_user_id, params.get("document_id")))
                self.docs_dbh.commit()
                self.send_response(200, {"error": 0})
        except Exception as e:
            print(str(e))
            self.send_response(500, {"error": 1})

    def get_doc_html(self, params):
        print("get_doc_html processing")
        try:
            query = "SELECT URL FROM documents_archive WHERE document_id='{}' ORDER BY revision_id DESC LIMIT 1" \
                .format(params.get("doc_id"))
            self.cursor.execute(query)
            path = self.cursor.fetchall()[0][0]
            if os.path.isfile(path):
                with open(path, 'r') as file:
                    self.send_response(200, {"error": 0, "response": file.read()})
            else:
                self.send_response(200, {"error": 0, "response": ''})
        except Exception as e:
            print(str(e))
            self.send_response(500, {'error': 1})

    def set_doc_html(self, params):
        print("set_doc_html processing")
        try:
            if params.get("text") == "skfngs[okedvbo[ske[othns[oedfbaokmspejvmaoeurh jpaow awoihoawihvnawh[ocmiwmehciw":
                params["text"] = ""
            self.cursor.execute(
                "SELECT employee_id FROM employee WHERE email='{}'".format(params.get("customer_email")))
            params['employee_id'] = self.cursor.fetchall()[0][0]
            self.cursor.execute("SELECT employee_id FROM employee WHERE email='{}'".format(params.get("sender")))
            sender = self.cursor.fetchall()[0][0]
            params["sender"] = sender
            params['noresponse'] = True
            self.add_agreement(params)
            last_agreement_id = self.get_last_agreement(id=params.get("document_id"))
            path = self.archive_folder + "doc_" + str(params.get("document_id")) + "_" + str(last_agreement_id)
            with open(path, "w") as file:
                file.write(params.get("text"))
            self.send_response(200, {"error": 0})
        except Exception as e:
            print(str(e))
            self.send_response(500, {'error': 1})

    def get_note(self, params):
        query = "SELECT note FROM documents_archive WHERE document_id='{}' ORDER BY revision_id DESC LIMIT 1" \
            .format(params.get("document_id"))
        try:
            self.cursor.execute(query)
            response = self.cursor.fetchall()[0][0]
            self.send_response(200, {"error": 0, "response": response})
        except Exception as e:
            print(str(e))
            self.send_response(500, {"error": 1})

    def send_response(self, code, response):
        try:
            self.conn.send((self.header_gen(code) + json.dumps(response)).encode())
            self.conn.close()
        except Exception as e:
            print(str(e))

    def stop_server(self, exc):
        try:
            self.conn.close()
        except Exception as e:
            print(str(e))
        self.sock.close()
        print("Server stopped. " + str(exc))
        sys.exit(0)

    @staticmethod
    def header_gen(status):
        header = ''
        if status == 200:
            header += 'HTTP/1.1 200 OK\n'
        elif status == 201:
            header += 'HTTP/1.1 201 Created\n'
        elif status == 404:
            header += 'HTTP/1.1 404 Not Found\n'
        elif status == 500:
            header += 'HTTP/1.1 500 Internal Server Error\n'
        current_time = time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime())
        header += "Date: {}\n".format(current_time)
        header += "Server: notesRest\n"
        header += "Connection: close\n\n"
        return header


server = None


def start_server(port):
    global server
    server = Server(port)
    server.start_server()


if __name__ == "__main__":
    start_server(7788)
