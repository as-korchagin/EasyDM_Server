-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: easydm
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `agreement`
--

DROP TABLE IF EXISTS `agreement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agreement` (
  `agreement_id` int(11) NOT NULL AUTO_INCREMENT,
  `document_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `final_user` int(11) DEFAULT NULL,
  `agreement_date` varchar(256) DEFAULT NULL,
  `agreement_result` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`agreement_id`),
  KEY `agreement_document_document_id_fk` (`document_id`),
  KEY `agreement_employee_employee_id_fk` (`employee_id`),
  KEY `agreement_employee_employee_id_fk_2` (`final_user`),
  CONSTRAINT `agreement_document_document_id_fk` FOREIGN KEY (`document_id`) REFERENCES `document` (`document_id`),
  CONSTRAINT `agreement_employee_employee_id_fk` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`),
  CONSTRAINT `agreement_employee_employee_id_fk_2` FOREIGN KEY (`final_user`) REFERENCES `employee` (`employee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agreement`
--

LOCK TABLES `agreement` WRITE;
/*!40000 ALTER TABLE `agreement` DISABLE KEYS */;
INSERT INTO `agreement` VALUES (33,20,2,2,'2018-06-18 12:01:23.499181','agreed'),(34,21,2,NULL,'2018-06-18 12:16:03.819011','not agreed');
/*!40000 ALTER TABLE `agreement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document` (
  `document_id` int(11) NOT NULL AUTO_INCREMENT,
  `document_type_id` int(11) NOT NULL,
  `document_name` varchar(256) NOT NULL,
  `document_desc` text NOT NULL,
  PRIMARY KEY (`document_id`),
  KEY `document_document_type_document_type_id_fk` (`document_type_id`),
  CONSTRAINT `document_document_type_document_type_id_fk` FOREIGN KEY (`document_type_id`) REFERENCES `document_type` (`document_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document`
--

LOCK TABLES `document` WRITE;
/*!40000 ALTER TABLE `document` DISABLE KEYS */;
INSERT INTO `document` VALUES (20,7,'new','new'),(21,7,'new','new');
/*!40000 ALTER TABLE `document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document_type`
--

DROP TABLE IF EXISTS `document_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_type` (
  `document_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `document_type_name` varchar(256) NOT NULL,
  PRIMARY KEY (`document_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document_type`
--

LOCK TABLES `document_type` WRITE;
/*!40000 ALTER TABLE `document_type` DISABLE KEYS */;
INSERT INTO `document_type` VALUES (1,'Организационно-правовая документация'),(2,'Плановая документация'),(3,'Распорядительная документация'),(4,'Информационно-справочная документация'),(5,'Справочно-аналитическая документация'),(6,'Отчетная документация'),(7,'Документация по обеспечению кадрами'),(8,'Финансовая документация'),(9,'Документация по материально-техническому обеспечению'),(10,'Договорная документация');
/*!40000 ALTER TABLE `document_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents_archive`
--

DROP TABLE IF EXISTS `documents_archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents_archive` (
  `revision_id` int(11) NOT NULL AUTO_INCREMENT,
  `document_id` int(11) DEFAULT NULL,
  `note` text,
  `sender` varchar(256) DEFAULT NULL,
  `URL` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`revision_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents_archive`
--

LOCK TABLES `documents_archive` WRITE;
/*!40000 ALTER TABLE `documents_archive` DISABLE KEYS */;
INSERT INTO `documents_archive` VALUES (31,20,'New document','1','/media/andrey/LinuxHDD/doc_archive/doc_20_31'),(32,20,'only you can agree this','1','/media/andrey/LinuxHDD/doc_archive/doc_20_32'),(33,20,'no, you will','2','/media/andrey/LinuxHDD/doc_archive/doc_20_33'),(34,21,'New document','2','/media/andrey/LinuxHDD/doc_archive/doc_21_34');
/*!40000 ALTER TABLE `documents_archive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `employee_id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_name` varchar(256) NOT NULL,
  `position_id` int(11) NOT NULL,
  `login` varchar(256) NOT NULL,
  `pass` varchar(256) NOT NULL,
  `phone` varchar(64) NOT NULL,
  `email` varchar(128) NOT NULL,
  `admin` int(11) DEFAULT '0',
  PRIMARY KEY (`employee_id`),
  KEY `employee_position_position_id_fk` (`position_id`),
  CONSTRAINT `employee_position_position_id_fk` FOREIGN KEY (`position_id`) REFERENCES `position` (`position_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1,'Employee_1',1,'employee_1','12345678','+79261234567','employee_1@company.ru',1),(2,'Employee_2',5,'employee_2','12345678','+79261234567','employee_2@company.ru',0),(3,'Иванов Иван Иванович',10,'ivanov','12345678','+79160000000','ivanov@company.ru',0);
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `position`
--

DROP TABLE IF EXISTS `position`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `position` (
  `position_id` int(11) NOT NULL AUTO_INCREMENT,
  `position_name` varchar(256) NOT NULL,
  `salary` int(11) NOT NULL,
  PRIMARY KEY (`position_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `position`
--

LOCK TABLES `position` WRITE;
/*!40000 ALTER TABLE `position` DISABLE KEYS */;
INSERT INTO `position` VALUES (1,'Генеральный директор',200000),(2,'Исполнительный директор',200000),(3,'Финансовый директор',200000),(4,'Главный бухгалтер',150000),(5,'Главный инженер',150000),(6,'GUI team leader',150000),(7,'Backend team leader',150000),(8,'GUI разработчик',100000),(9,'Младший GUI разработчик',60000),(10,'Backend разработчик',100000),(11,'Младший Backend разработчик',60000),(12,'Обслуживающий персонал',40000);
/*!40000 ALTER TABLE `position` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-18 14:50:16
